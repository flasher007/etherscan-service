import axios, {
  Method,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosProxyConfig,
  AxiosError,
  ResponseType,
  AxiosResponse,
  AxiosBasicCredentials,
  AxiosTransformer,
} from 'axios';
import { get, set, omit, pick } from 'lodash';
import { DefaultError } from './CreateError';

//
type HTTPClientProps = {
  baseURL: string;
  threads?: number;
  timeout?: number;
  cachePeriod?: number;
  headers?: any;
  proxy?: AxiosProxyConfig;
  withCredentials?: boolean;
  databaseLogging?: boolean | Function;
  logEachErrorToSentry?: boolean;
  auth?: AxiosBasicCredentials;
  validateStatus?: (status: number) => boolean;
  getDataFromResponse?: (response: AxiosResponse) => any;
  getErrorFromResponse?: (error: AxiosError) => any;
  transformRequest?: AxiosTransformer | AxiosTransformer[];
  transformResponse?: AxiosTransformer | AxiosTransformer[];
};

export interface RequestConfig extends AxiosRequestConfig {}

type AxiosHeaders = {
  responseType?: ResponseType;
};

type AxiosAgentProps = {
  proxy?: AxiosProxyConfig;
};

//
const SECURE_FIELDS = new Set([
  'config.auth.password',
  'config.auth.username',
  'config.data.password',
  'config.data.token',
  'config.headers.Authorization',
]);

//
class HTTPClient {
  client: AxiosInstance;
  cachePeriod: number;
  getDataFromResponse: (response: AxiosResponse) => any;
  getErrorFromResponse: (error: AxiosError) => any;
  validateStatus: (status: number) => boolean;
  pendingRequests: number = 0;
  baseURL?: string;

  constructor(props: HTTPClientProps) {
    const {
      timeout = 3000, // by default we cache all requests for 3 seconds
      cachePeriod = 0,
      // "proxy" - объект, который имеет обязательные поля "host" и "port
      // а также необязательное поле "auth", которое если указано, то должно быть объектом
      // с полями user и password
      proxy,
      headers,
      // Функция, которую можно объявить для обозначения логики формирования
      // отформатированного объекта ошибки
      getErrorFromResponse,
      getDataFromResponse,
      baseURL,
      transformRequest,
      transformResponse,
      validateStatus,
      // Concurrency settings
      threads = 60,
      // Basic auth params
      auth,
      withCredentials,
      //
      logEachErrorToSentry = true,
    } = props;
    let {
      // Should we save queryLogs to database
      databaseLogging = false,
    } = props;
    //
    this.baseURL = baseURL;
    this.cachePeriod = cachePeriod;
    //
    this.getDataFromResponse =
      typeof getDataFromResponse === 'function'
        ? getDataFromResponse
        : res => res.data;
    //
    this.getErrorFromResponse =
      typeof getErrorFromResponse === 'function'
        ? getErrorFromResponse
        : error => {
            const code =
              get(error, 'response.data.code') ||
              get(error, 'response.error.code') ||
              error.code;
            const status =
              get(error, 'response.data.status') ||
              get(error, 'response.error.status') ||
              get(error, 'status') ||
              0;
            const statusText = get(error, 'response.statusText');
            const message =
              get(error, 'response.data.message') ||
              get(error, 'response.error.message') ||
              error.message ||
              statusText;
            const {
              serviceName = baseURL || 'unknown-service',
              errorCode = code || 'unknown-error-code',
              description = message,
            } = get(error, 'response.data', {});
            //
            return new DefaultError(message, {
              status,
              meta: {
                code,
                statusText,
                serviceName,
                errorCode,
                description,
                ...pick(get(error, 'config') || {}, ['data', 'url', 'method']),
                // @ts-ignore
                ...(error.data || {}),
              },
              originalError: error,
            });
          };
    //
    const clientProps: AxiosRequestConfig = {
      baseURL,
      timeout,
      proxy: false,
      auth,
      withCredentials,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'User-Agent': 'node/1.0.0',
        ...headers,
      },
    };
    if (typeof validateStatus === 'function') {
      clientProps.validateStatus = validateStatus;
    }
    if (typeof transformRequest === 'function') {
      clientProps.transformRequest = [transformRequest];
    }
    if (typeof transformResponse === 'function') {
      clientProps.transformResponse = [transformResponse];
    }
    if (proxy) {
      const agentProps: AxiosAgentProps = {};
      agentProps.proxy = {
        host: get(proxy, 'host'),
        port: get(proxy, 'port'),
      };
      //
      const userAgent = get(proxy, 'userAgent');
      if (userAgent) {
        clientProps.headers['User-Agent'] = userAgent;
      }
      //
      const proxyAuth = get(proxy, 'auth');
      if (proxyAuth) {
        const { username, password } = proxyAuth;
        // @ts-ignore
        agentProps.proxy.proxyAuth = `${username}:${password}`;
      }
    }
    //
    this.client = axios.create(clientProps);
    //
    this.client.interceptors.request.use(config => {
      return new Promise((resolve, reject) => {
        const interval = setInterval(() => {
          if (this.pendingRequests < threads) {
            this.pendingRequests += 1;
            clearInterval(interval);
            resolve(config);
          }
        }, 100);
      });
    });

    this.client.interceptors.response.use(
      async response => {
        this.pendingRequests = Math.max(0, this.pendingRequests - 1);
        //
        if (!databaseLogging) {
          return response;
        }
        if (typeof databaseLogging === 'function') {
          const shouldLog = databaseLogging(response);
          if (!shouldLog) {
            return response;
          }
        }
        //
        const queryConfig: RequestConfig = pick(get(response, 'config', {}), [
          'method',
          'baseURL',
          'url',
          'params',
          'data',
          'headers',
        ]);
        const { method, url = '', params = {} } = queryConfig;
        let { data = {} } = queryConfig;
        //
        const filteredHeaders = {};
        //
        Object.keys(queryConfig.headers).forEach(headerKey => {
          //
          const privateHeader =
            headerKey.toLowerCase().includes('key') ||
            headerKey.toLowerCase().includes('token') ||
            headerKey.toLowerCase().includes('password');
          if (privateHeader) {
            // @ts-ignore
            filteredHeaders[headerKey] = '[FILTERED]';
          } else {
            // @ts-ignore
            filteredHeaders[headerKey] = queryConfig.headers[headerKey];
          }
        });
        try {
          data =
            typeof queryConfig.data === 'string' && queryConfig.data
              ? JSON.parse(queryConfig.data)
              : queryConfig.data;
        } catch (requestDataParseError) {
          // Ignore data-parse error
        }
        //
        const queryResponse = {
          status: get(response, 'status', 0),
          statusText: get(response, 'statusText', ''),
          method,
          baseURL: queryConfig.baseURL,
          url,
          params,
          data,
          config: queryConfig,
          headers: filteredHeaders,
          response: omit(response, ['request', 'config']),
          // ip,
          // userId,
        };
        //
        return response;
      },
      async (err: AxiosError) => {
        this.pendingRequests = Math.max(0, this.pendingRequests - 1);
        //
        //
        const queryConfig: RequestConfig = pick(get(err, 'config', {}), [
          'method',
          'baseURL',
          'url',
          'params',
          'data',
          'headers',
        ]);
        const { method, url = '', params = {}, data = {} } = queryConfig;
        const queryError = {
          status: get(err, 'status', 0),
          statusText: get(err, 'statusText', ''),
          method,
          baseURL: queryConfig.baseURL,
          url,
          params,
          data,
          config: queryConfig,
          headers: queryConfig.headers,
          error: err.response
            ? err.response.data
            : omit(err, ['request', 'config']),
          // ip,
          // userId,
        };
        //
        SECURE_FIELDS.forEach(secureField => {
          //
          if (get(err, secureField)) {
            set(err, secureField, '[Filtered]');
          }
        });
        //
        throw err;
      },
    );
  }

  static makeQueryString(query: any) {
    //
    if (!query) {
      return '';
    }
    return `?${Object.keys(query)
      .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(query[k])}`)
      .join('&')}`;
  }

  async sendRequest(
    m: Method,
    url: string,
    data?: any,
    headers?: AxiosHeaders,
    config?: { cachePeriod: number },
  ) {
    const { getDataFromResponse, getErrorFromResponse } = this;
    const cachePeriod =
      typeof config?.cachePeriod === 'number'
        ? config.cachePeriod
        : this.cachePeriod;

    // @ts-ignore
    const method: Method = m.toUpperCase();
    try {
      let cacheKey;
      // if (method === 'GET' && redis && cachePeriod) {
      //   cacheKey = `${method}_${url}${data ? `_${JSON.stringify(data)}` : ''}`;
      //   const res = await redis.getAsync(cacheKey);
      //   if (res) {
      //     return JSON.parse(res);
      //   }
      // }
      const { responseType, ...otherHeaders } = headers || {};
      const requestOptions: RequestConfig = {
        url,
        method,
        headers: otherHeaders,
      };
      if (responseType) {
        requestOptions.responseType = responseType;
      }
      if (method === 'GET') {
        requestOptions.params = data;
      } else {
        requestOptions.data = data;
      }
      // console.log(this.baseURL, 'requestOptions', requestOptions);
      const res = await this.client(requestOptions);
      const responseData = getDataFromResponse(res);
      // console.log('responseData', responseData);
      //
      // if (method === 'GET' && redis && cachePeriod) {
      // PX - miliseconds;
      // redis.set(cacheKey, JSON.stringify(responseData), 'PX', cachePeriod);
      // }
      return responseData;
    } catch (error) {
      // console.log(error);
      throw getErrorFromResponse(error);
    }
  }
}

export default HTTPClient;
