export class ITransaction {
  blockNumber: number;

  blockHash: string;

  hash: string;

  from: string;

  to: string;

  value: string;

  gasPrice: string;
}
