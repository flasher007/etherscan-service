import { Inject, Injectable, Logger } from '@nestjs/common';
import { Interval } from '@nestjs/schedule';
import { Op } from 'sequelize';
import { get } from 'lodash';
import EtherscanAPIClient from './EtherscanAPIClient';
import { ConfigService } from '../../shared/config/ConfigService';
import { TransactionService } from './TransactionService';
import { toHex, hexToNumber } from 'web3-utils';

const DEFAULT_START_BLOCK = 9842805;

@Injectable()
export class EtherscanService {
  private readonly logger = new Logger(EtherscanService.name);

  constructor(
    private readonly configService: ConfigService,
    private readonly service: TransactionService,
  ) {}

  @Interval(15000)
  async handleCron() {
    this.logger.debug('Start');

    try {
      const api = new EtherscanAPIClient({
        apiKey: this.configService.etherscanApiKey,
      });
      const tx = await this.service.getLast();
      const blockNumber = get(tx, 'blockNumber')
        ? get(tx, 'blockNumber') + 1
        : DEFAULT_START_BLOCK;
      const { transactions } = await api.getBlockByNumber({
        tag: toHex(blockNumber),
      });
      await this.service.add(transactions);
      // console.log(blockNumber.toString(16), transactions);
      this.logger.log(
        `[PARSE BLOCK] In block ${blockNumber}|${toHex(blockNumber)} find ${
          transactions.length
        } transactions`,
      );
    } catch (e) {
      console.log(e);
      this.logger.error(`[ERROR] ${e.message}`);
      throw new Error(e);
    }
  }
}
