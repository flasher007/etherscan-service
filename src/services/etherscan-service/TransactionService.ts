import { get } from 'lodash';
import { Transaction } from '../../shared/entity/Transaction';
import { ITransaction } from '../../shared/interface/ITransaction';
import { Decimal } from 'decimal.js';
import { fromWei, toWei, toBN, toDecimal, hexToNumber } from 'web3-utils';

const wei = 1000000000000000000;
export type EtherscanTransaction = {
  blockHash: string;
  blockNumber: string;
  from: string;
  gas: string;
  gasPrice: string;
  hash: string;
  input: string;
  nonce: string;
  to: string;
  transactionIndex: string;
  value: string;
  v: string;
  r: string;
  s: string;
};

export class TransactionService {
  async getLast() {
    return Transaction.findOne({
      order: [['blockNumber', 'DESC']],
    });
  }

  async add(txList: EtherscanTransaction[]) {
    try {
      const formatTxs = txList.map(tx => this.formatTx(tx));
      return Transaction.bulkCreate(formatTxs);
    } catch (e) {
      throw new Error(e);
    }
  }

  formatTx(tx: EtherscanTransaction): ITransaction {
    return {
      blockHash: get(tx, 'blockHash'),
      blockNumber: toDecimal(get(tx, 'blockNumber')),
      from: get(tx, 'from', ''),
      gasPrice: this._formatAmount(get(tx, 'gasPrice')),
      hash: get(tx, 'hash'),
      to: get(tx, 'to', ''),
      value: this._formatAmount(get(tx, 'value')),
    };
  }

  _formatAmount(amount: string) {
    return new Decimal(amount).div(wei).toFixed(8);
    // const v = toBN(fromWei(`${hexToNumber(amount)}`)).toNumber();
  }
}
