import { Global, Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { EtherscanService } from './EtherscanService';
import { ConfigService } from '../../shared/config/ConfigService';
import { databaseProviders } from '../../database/databaseProviders';
import { TransactionService } from './TransactionService';

@Global()
@Module({
  providers: [
    ...databaseProviders,
    ConfigService,
    EtherscanService,
    TransactionService,
  ],
  exports: [],
  imports: [ScheduleModule.forRoot()],
  controllers: [],
})
export class EtherscanServiceModule {}
