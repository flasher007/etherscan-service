import { Bootstrap } from '../Bootstrap';
import { Config } from './config';
import { EtherscanServiceModule } from './EtherscanServiceModule';

Bootstrap.context(EtherscanServiceModule, Config);
