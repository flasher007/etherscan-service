import { Module } from '@nestjs/common';
import { databaseProviders } from './databaseProviders';
import { ConfigService } from '../shared/config/ConfigService';

@Module({
  providers: [...databaseProviders, ConfigService],
  exports: [...databaseProviders],
})
export class DatabaseModule {}
