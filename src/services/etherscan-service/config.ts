import { IServiceConfig } from '../../shared/interface/IServiceConfig';

export const Config: IServiceConfig = {
  serviceName: process.env.DEPOSIT_SERVICE_NAME || 'EtherscanService',
};
