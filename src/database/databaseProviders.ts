import { Sequelize } from 'sequelize-typescript';
import { ConfigService } from '../shared/config/ConfigService';
import { Transaction } from '../shared/entity/Transaction';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async (configService: ConfigService) => {
      const sequelize = new Sequelize(configService.sequelizeOrmConfig);
      sequelize.addModels([Transaction]);
      await sequelize.sync();
      return sequelize;
    },
    inject: [ConfigService],
  },
];
