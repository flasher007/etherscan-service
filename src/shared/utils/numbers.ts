import BigNumber from 'bignumber.js';

// @ts-ignore
BigNumber.config({ ERRORS: false });

export function formatMoneyValue(
  value: string | number,
  decimalPlaces: number,
) {
  return parseFloat(new BigNumber(value).toFixed(decimalPlaces, 3));
}

export default BigNumber;
