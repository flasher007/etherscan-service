import {
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';

@Table({
  tableName: 'transactions',
})
export class Transaction extends Model<Transaction> {
  @Column({
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column
  blockNumber: number;

  @Column
  blockHash: string;

  @Column
  hash: string;

  @Column
  from: string;

  @Column
  to: string;

  @Column({ type: DataType.DECIMAL(18, 9) })
  value: number;

  @Column({ type: DataType.DECIMAL(18, 9) })
  gasPrice: number;

  @CreatedAt
  @Column
  createdAt: Date;

  @UpdatedAt
  @Column
  updatedAt: Date;
}
