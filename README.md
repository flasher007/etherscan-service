## Etherscan Monitoring Service

### Запуск:
```
npm i
```

Скопировать .env и прописать настройки для БД
```
cp .env.example .env
```

```
npm run db:migration
```

```
npm run etherscan-service
```


P.S. не успел сделать метод АПИ, который выводит адрес, баланс которого изменился сильнее всего начиная с 9842805 блока.
