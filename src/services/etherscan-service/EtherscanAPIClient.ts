/* eslint-disable lines-between-class-members */
import { get } from 'lodash';
import HTTPClient from '../../shared/HttpClient';
import { DefaultError } from '../../shared/CreateError';

//
export type EtherscanAPIClientProps = {
  apiKey: string;
};

export type EtherscanOptimalFee = {
  lastBlock: number;
  optimalGasPrice: number;
  safeGasPrice: number;
};

class EtherscanAPIClient {
  apiKey: string;
  client: HTTPClient;

  constructor(props: EtherscanAPIClientProps) {
    const { apiKey } = props;
    //
    if (!apiKey) {
      throw new Error('Please, set "apiKey"');
    }
    //
    this.apiKey = apiKey;
    this.client = new HTTPClient({
      baseURL: 'https://api.etherscan.io/api',
      timeout: 30000,
      threads: 10,
      cachePeriod: 10000, // cache all GET queries for 10 seconds, by default
      logEachErrorToSentry: false,
      getDataFromResponse(response) {
        const error = get(response, 'data.error');
        const result = get(response, 'data.result');
        if (!error && result) {
          return result;
        }
        if (error) {
          throw error;
        }
        throw response;
      },
    });
  }

  async request(action, module, params = {}) {
    //
    try {
      const res = await this.client.sendRequest('get', '/', {
        action,
        module,
        apiKey: this.apiKey,
        ...params,
      });
      return res;
    } catch (e) {
      throw new DefaultError(e.message, {
        meta: {
          method: 'GET',
          url: `${this.client.baseURL}/${action}`,
          module,
          params,
        },
        originalError: e,
      });
    }
  }

  async getLastBlockNumber() {
    return this.request('eth_blockNumber', 'proxy');
  }

  async getBlockByNumber({ tag = 'latest', isBoolean = true }) {
    return this.request('eth_getBlockByNumber', 'proxy', {
      tag,
      boolean: isBoolean,
    });
  }
}

export default EtherscanAPIClient;
